#TehnoxpertProject

**INTRODUCTION:**

   Automation testing of different REST API requests such as POST,PUT,PATCH,GET and DELETE.

**FEATURES:**

1. Here we used sample free APIs from reqres.in website for api testing demonstartion.

2. Created environment as reqres_env,used environment variables and executed all APIs in that environment

3. Used different testing methods such as data driven testing using json data file and excel data file,
   tested APIs using dyanamic varaiables also

4. Used concept of request chaining in one of the POST API ,executed for 5 times and then terminated the loop   
5. Used newman,html reporter and html reporter extra to generate html reports.















